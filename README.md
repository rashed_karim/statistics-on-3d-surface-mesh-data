# README #

A simple application that uses VTK and Boost libraries to read a polydata shell with point data scalars and computes the mean, median and variance of the scalars. 

### What is this repository for? ###

* Mean, Median and Variance of point data scalars in VTK surfaces 


### How do I get set up? ###

* Setting up 
Please see the CMake file, expects Homebrew install of Boost. Otherwise modification of CMake may be necessary. 
* Dependencies 
VTK 6, Boost 1.58 

### Contribution guidelines ###

* Writing tests
Tested on a few VTK files. No proper validation conducted. 


### Who do I talk to? ###

* Please e-mail: rashed.karim@gmail.com