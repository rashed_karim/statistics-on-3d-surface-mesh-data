#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPointLocator.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

// Boost libraries
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>


#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

// The boost in this code has adopted from 
// http://theboostcpplibraries.com/boost.accumulators
using namespace boost::accumulators;
 
/*
*   Reads scalars from a polydata shell  
*/
void GetScalars(char* poly_fn, vector<double>& output, double t)
{
  vtkSmartPointer<vtkPolyData> surface =vtkSmartPointer<vtkPolyData>::New();  
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader->SetFileName(poly_fn); 
  reader->Update();
  
  surface = reader->GetOutput();
  
  vtkSmartPointer<vtkFloatArray> scalars = vtkSmartPointer<vtkFloatArray>::New();
  scalars = vtkFloatArray::SafeDownCast(surface->GetPointData()->GetScalars());  
  
  for (int k=0;k<surface->GetNumberOfPoints();k++)
  {
    double s = scalars->GetTuple1(k); 
    if (s > t)
      output.push_back(s); 
  }
}
 
int main(int argc, char **argv)
{
  
  vector<double> scalar;
  char* poly_fn;
  
  if (argc < 2)
  {
     cerr << "Not enough parameters\nUsage: <file.vtk>"<< endl; 
     exit(1);
  }
  
  poly_fn = argv[1];
  
  cout << "Reading file : " << poly_fn << endl;
  
   
  GetScalars(poly_fn, scalar, 0);
  
  // Please see: http://theboostcpplibraries.com/boost.accumulators
  accumulator_set<double, features<tag::mean, tag::variance, tag::median> > acc;
  
  for (int i=0;i<scalar.size();i++) {
    acc(scalar[i]);
  }
  
  cout << "\nMedian = " << median(acc) << "\nMean = " << mean(acc) << "\nVariance = " << variance(acc) << endl;
  
  
}